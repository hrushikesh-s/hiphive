.. _related_codes:
.. index:: Related Codes

.. |br| raw:: html

  <br/>


Related Codes
*************

* `alamode <https://sourceforge.net/projects/alamode/>`_
* `almaBTE <https://almabte.bitbucket.io/>`_
* `CSLD <https://github.com/LLNL/csld>`_
* `fhi-vibes <https://vibes-developers.gitlab.io/vibes/>`_
* `GPUMD <https://github.com/brucefan1983/GPUMD>`_
* `Kaldo <https://github.com/nanotheorygroup/kaldo>`_
* `PHONON <http://www.computingformaterials.com/>`_
* `phonopy <https://phonopy.github.io/phonopy/>`_
* `phono3py <https://phonopy.github.io/phono3py/>`_
* `shengBTE <https://bitbucket.org/sousaw/shengbte/>`_
* `TDEP <https://ollehellman.github.io/>`_
* `SSCHA <https://sscha.eu/>`_
