.. index:: Glossary

Glossary
********

General
=======
.. glossary::

   BCC
        Several metallic including for example elements from groups 5 (V, Nb, Ta) and 6 (Cr, Mo, W) have a `body-centered cubic (BCC) <https://en.wikipedia.org/wiki/Cubic_crystal_system>`_ ground state structure.

   FCC
        The `face-centered cubic (FCC) lattice <https://en.wikipedia.org/wiki/Cubic_crystal_system>`_ is one of the most common crystal structures for metallic elements including e.g., the late transition metals from group 10 (Ni, Pd, Pt) and 11 (Cu, Ag, Au).

   DFT
        The construction of force constants requires accurate reference data.
        `Density functional theory (DFT) <https://en.wikipedia.org/wiki/Density_functional_theory>`_ calculations are one of the most common source for such data.

   NAC
        The non-analytic term correction (NAC) is required in (at least partially) ionic materials in order to restore the correct splitting of the longitudinal optical (LO) and transverse optical (TO) modes in the long-wave length limit (:math:`\vec{q}\rightarrow\vec{0}`).

   CV
   cross validation
        `Cross validation (CV)
        <https://en.wikipedia.org/wiki/Cross-validation_(statistics)>`_
        is commonly employed to evaluated the transferability and accuracy of
        linear problems.

   regularization
        `Regularization
        <https://en.wikipedia.org/wiki/Regularization_(mathematics)>`_,
        is commonly used in machine learning to combat overfitting and
        for solving underdetermined systems.

   RMSE
        `Root mean square error
        <https://en.wikipedia.org/wiki/Root-mean-square_deviation>`_,
        is a frequently measure for the deviation between a reference
        data set and a predicted data set.


Crystal symmetry and clusters
=============================
.. glossary::

   crystal symmetry operation
        A crystal symmetry operation for a specific lattice means that the
        lattice is invariant under this operation. An operation comprises
        translational and rotational components.

   cluster
        A cluster is defined as a set of points on a lattice.

   cluster size
        The size of a cluster (commonly refered to as the cluster radius) is
        defined as the average distance to the geometrical center of the cluster.

   cluster space
        The set of clusters into which a structure can be decomposed.

   cutoff
        Cutoffs define the longest allowed distance between two atoms in a
        cluster for each order.

   orbit
        An orbit is defined as a set of symmetry equivalent clusters

   orientation family
        An orientation family is a subgroup to an orbit, which contains those
        clusters that are oriented identically in the lattice.


Force constants
===============
.. glossary::

   irreducible parameters
        Many elements of the force constant matrices are related to each other
        by symmetry operations. The irreducible set up of parameters is
        obtained by applying all symmetry operations allowed by the space group
        of the ideal lattice and the sum rules.

   sum rules
        In order for a force constant potential to fulfill translational
        invariance certain constraints are imposed on the force constants.
        These constraints are commonly referred to as sum rules.
